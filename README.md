Dane pobrać z https://restcountries.eu/#api-endpoints-code
• Widok.0 /ekran główny - Na głównym widoku wyświetlić regiony:
• Afryka,
• Ameryka,
• Azja,
• Europa,
• Oceania
(w postaci listy lub kafelków, inwencja twórcza mile widziana) Po kliknięciu danego regionu przechodzimy do widoku.1
• Widok.1 - lista Proszę wyświetlić listę państw
• elementy listy powinny zawierać flagę, nazwę państwa
• możliwość powrotu do widoku0 /ekranu głównego
• Po kliknięciu na element listy, przechodzimy do widoku (widok.2) danego państwa
• Widok.2 - szczegółowe dane o wybranym państwie
• wyświetlić dodatkowe dane: waluta, stolice itp.
• możliwość powrotu do widoku 2
Informacje dodatkowe :
Logo i przycisk „o mnie” nie są wymagane, ale mile widziane
• logo - komponent z grafiką
• przycisk „o mnie” - który po kliknięciu pokaże modal/popup z informacjami o autorze.
• Proszę wykonać zadanie jakby było częścią dużego projektu.
• Pochwal się wiedzą i technologiami.
• Wykorzystaj elementy architektury Angulara( moduły /komponenty ) i odpowiednio je pogrupuj.
Poniżej znajdziesz szkic projektu

- zastosować lazy loading,
- zastosować triki poprawiajace preformance (wydajnosc aplikacji), o których mówiliśmy na rozmowie,
- routing aplikacji - chcemy mieć mozliwość wejścia w informacje bezpośrednio z linku,
- preferujemy nazwy zmienych i funkcji po angielsku,
- do typowania użyć trybu strict.
