export interface Region {
  name?: string,
  icon?: string,
  link?: string
}
