import { Pipe, PipeTransform } from "@angular/core";
import { Country } from "../models/country.model";

@Pipe({
  name: "countriesPipe",
  pure: false
})
export class CountriesPipe implements PipeTransform {
  transform(countries: Country[], name: string): Country[] {
    return name ? countries.filter((country) => {
      return country.name.toLowerCase().includes(name.toLowerCase());
    }) : countries;
  }
}
