import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Country } from '../models/country.model';

@Injectable({
  providedIn: 'root'
})
export class CountriesService {
  private url = 'http://api.countrylayer.com/v2/';
  private apiKey = 'baa0dd3f8225e7b31cc2370f41b50e27';

  constructor(private http: HttpClient) { }

  getCountriesByRegion(countryRegion: string): Observable<Country[]> {
    return countryRegion ? this.http.get<Country[]>(this.url + 'region/' + countryRegion + '?access_key=' + this.apiKey) : this.http.get<Country[]>(this.url + 'all' + '?access_key=' + this.apiKey);
  }

  getCountryByName(countryName: string): Observable<Country> {
    return this.http.get<Country>(this.url + 'name/' + countryName + '?access_key=' + this.apiKey);
  }


}
