import { BehaviorSubject, Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { Region } from '../models/region.model';

@Injectable({
  providedIn: 'root'
})
export class RegionService {

  regions: Region[] = [
    {
      name: 'Africa',
      icon: 'africa.jpg',
      link: 'africa'
    },
    {
      name: 'Americas',
      icon: 'americas.jpg',
      link: 'americas'
    },
    {
      name: 'Asia',
      icon: 'asia.jpg',
      link: 'asia'
    },
    {
      name: 'Europe',
      icon: 'europe.jpg',
      link: 'europe'
    },
    {
      name: 'Oceania',
      icon: 'oceania.jpg',
      link: 'oceania'
    },
    {
      name: 'World',
      icon: 'globe.jpg',
      link: ''
    },
  ];

  constructor() { }

  getAllRegions(): Observable<Region[]> {
    return of(this.regions);
  }
}
