import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material.module';
import { LazyRoutingModule } from './lazy-routing.module';
import { CountryListComponent } from '../components/country-list/country-list.component';
import { FormsModule } from '@angular/forms';
import { CountriesPipe } from '../pipes/countries.pipe';
@NgModule({
  declarations: [
    CountryListComponent,
    CountriesPipe
  ],
  imports: [
    CommonModule,
    MaterialModule,
    LazyRoutingModule,
    FormsModule
  ]
})
export class LazyModule { }
