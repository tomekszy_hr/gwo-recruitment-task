import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CountryListComponent } from '../components/country-list/country-list.component';
import { CountryDetailsComponent } from '../components/country-details/country-details.component';

const routes: Routes = [
  { path: 'countryList/:region', component: CountryListComponent },
  { path: 'countryDetails/:countryName', component: CountryDetailsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LazyRoutingModule { }
