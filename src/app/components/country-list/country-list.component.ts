import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { CountriesService } from 'src/app/services/countries.service';
import { Country } from 'src/app/models/country.model';
import { Subscription, Observable } from 'rxjs';

@Component({
  selector: 'app-country-list',
  templateUrl: './country-list.component.html',
  styleUrls: ['./country-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CountryListComponent implements OnInit {
  private region: string;
  private routeSub: Subscription;
  public term: string;
  public countries$: Observable<Country[]>

  constructor(
    private route: ActivatedRoute,
    private countriesService: CountriesService,
  ) { }

  ngOnInit(): void {
    this.routeSub = this.route.paramMap.subscribe((paramMap: ParamMap) => {
      this.region = paramMap.get('region');
    });
    this.countries$ = this.countriesService.getCountriesByRegion(this.region);
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
  }
}
