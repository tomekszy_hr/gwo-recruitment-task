import { Country } from './../../models/country.model';
import { Subscription, Observable } from 'rxjs';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { CountriesService } from 'src/app/services/countries.service';

@Component({
  selector: 'app-country-details',
  templateUrl: './country-details.component.html',
  styleUrls: ['./country-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CountryDetailsComponent implements OnInit {
  routeSub: Subscription;
  countryName: string;
  country$: Observable<Country>;

  constructor(
    private route: ActivatedRoute,
    private countriesService: CountriesService,
  ) { }

  ngOnInit(): void {

    this.routeSub = this.route.paramMap.subscribe((paramMap: ParamMap) => {
      this.countryName = paramMap.get('countryName');
    });
    this.country$ = this.countriesService.getCountryByName(this.countryName);
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
  }
}
