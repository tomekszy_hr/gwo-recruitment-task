import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { RegionService } from 'src/app/services/region.service';
import { Region } from 'src/app/models/region.model';
@Component({
  selector: 'app-region-list',
  templateUrl: './region-list.component.html',
  styleUrls: ['./region-list.component.scss']
})
export class RegionListComponent implements OnInit {
  regions$: Observable<Region[]>;

  constructor(private regionService: RegionService) { }

  ngOnInit(): void {
    this.regions$ = this.regionService.getAllRegions();
  }

}
