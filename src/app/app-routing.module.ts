import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutComponent } from './components/about/about.component';
import { RegionListComponent } from './components/region-list/region-list.component';

const routes: Routes = [
  { path: '', component: RegionListComponent },
  { path: 'lazy', loadChildren: () => import('./modules/lazy.module').then(m => m.LazyModule) },
  { path: 'about', component: AboutComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
